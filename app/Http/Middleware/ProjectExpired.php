<?php

namespace App\Http\Middleware;

use Closure;

class ProjectExpired
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
				if ($response->original->project_info->days_left < 0) {
					return redirect()->back();
				} else {
					return $response;
				}
    }
}
