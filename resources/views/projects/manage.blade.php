<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width" />
		<title>Manage</title>
	</head>
	<body>
		<table style="border:1px #aaa solid;" cellpadding="10" rules="all">
			<tr>
				<td>Name</td>
				<td>Option</td>
				<td>amount</td>
			</tr>
			@foreach($project->donates->sortBy("amount")->sortBy("option_id") as $donate)
				<tr>
					<td>{{ $donate->name }}</td>
					<td>{{ $donate->option->title ?? "純贊助"}}</td>
					<td>{{ $donate->amount }}</td>
				</tr>
			@endforeach
		</table>
	</body>
</html>
